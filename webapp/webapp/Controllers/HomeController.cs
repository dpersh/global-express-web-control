﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webapp.DirectTV;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDirectTVRemote _directTVRemote;

        private class ActionMap
        {
            public string Name { get; set; }
            public Action<IDirectTVRemote> Func { get; set; }
        }


        public HomeController(IDirectTVRemote directTVRemote)
        {
            _directTVRemote = directTVRemote ?? throw new ArgumentNullException(nameof(directTVRemote));
        }
        // GET: /<controller>/
        //[HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        public IActionResult OnButtonClick(string button)
        {
            var buttonValue = TempData["buttonValue"];
            var actions = new Dictionary<string, Action>();

            actions.Add("power", () => _directTVRemote.Power());

            actions.Add("0", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit0));
            actions.Add("1", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit1));
            actions.Add("2", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit2));
            actions.Add("3", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit3));
            actions.Add("4", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit4));
            actions.Add("5", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit5));
            actions.Add("6", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit6));
            actions.Add("7", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit7));
            actions.Add("8", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit8));
            actions.Add("9", () => _directTVRemote.ChannelSet(DirectTvChannelDigit.ChannelSelectDigit9));

            actions.Add("enter", () => _directTVRemote.Enter());
            actions.Add("left", () => _directTVRemote.Left());
            actions.Add("right", () => _directTVRemote.Right());
            actions.Add("up", () => _directTVRemote.Up());
            actions.Add("down", () => _directTVRemote.Down());

            if(actions.ContainsKey(button.ToLower()))
            {
                actions[button.ToLower()].Invoke();
            }

            return RedirectToPage("/Index");
        }

    }
}
