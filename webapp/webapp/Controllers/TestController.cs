﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO.Ports;

namespace webapp.Controllers
{
    public class TestController : Controller
    {
        public string Index()
        {
            return "This is my default action...";
        }

        public string Welcome()
        {
            string[] portNames = SerialPort.GetPortNames();

            return "This is my Welcome action method...\n";
        }
        public string Serial()
        {
            string[] portNames = SerialPort.GetPortNames();


            string reply = "Serial ports:\n";

            foreach (var portName in portNames)
            {
                reply += (portName + "\n");
            }

            return reply;
        }

    }
}