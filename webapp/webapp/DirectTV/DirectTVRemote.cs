﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.DirectTV
{
    public class DirectTVRemote : IDirectTVRemote
    {
        private readonly IDirectTVInternal _control;

        public DirectTVRemote(IDirectTVInternal control)
        {
            _control = control ?? throw new ArgumentNullException(nameof(control));
        }
        public void ChannelUp()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.ChannelUp);
        }
        public void ChannelDown()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.ChannelDown);
        }
        public void ChannelSet(DirectTvChannelDigit digit)
        {
            var channel = (UInt16)(digit) + (UInt16)DirectTvSerialCommands.Command.Channel0;

            _control.SendCommand((UInt16)channel);
        }
        public void Power()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Power);
        }
        public void TranInfo()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.TranInfo);
        }
        public void Display()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Display);
        }
        public void Info()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Info);
        }

        public void LangSelect()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.LangSelect);
        }

        public void TvRadioToggle()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.TvRadio);
        }


        public void Setup()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Setup);
        }

        public void Guid()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Guid);
        }

        public void Up()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Up);
        }

        public void Down()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Down);
        }

        public void Left()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Left);
        }

        public void Right()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Right);
        }

        public void Exit()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Exit);
        }

        public void Enter()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Enter);
        }

        public void Menu()
        {
            _control.SendCommand((UInt16)DirectTvSerialCommands.Command.Menu);
        }


    }
}
