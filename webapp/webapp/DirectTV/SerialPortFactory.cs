﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace webapp.DirectTV
{
    public class SerialPortFactory : ISerialPortFactory
    {
        private SerialPort _serial;

        public SerialPortFactory()
        {
            // TODO: Make this configurable via json
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                _serial = new SerialPort("COM1");
            }
            else
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                _serial = new SerialPort("/dev/ttyS1");
            }
        }

        public SerialPort GetInstance()
        {
            return _serial;
        }
    }
}
