﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO.Ports;

namespace webapp.DirectTV
{
    public class DirectTVInternal : IDirectTVInternal
    {
        private readonly SerialPort _serialPort;

        public DirectTVInternal(ISerialPortFactory serailPortFactory)
        {
            if (serailPortFactory == null)
                throw new ArgumentNullException(nameof(serailPortFactory));

            _serialPort = serailPortFactory.GetInstance() ?? throw new ArgumentNullException(nameof(serailPortFactory));
        }
        public void SendCommand(UInt16 command)
        {
            if (!_serialPort.IsOpen)
            {
                _serialPort.Open();
                Console.WriteLine("> _serialPort.Open()");
            }

            const UInt16 MTU_MASK_LO = 0x00FF;
            const UInt16 MTU_MASK_HI = 0xFF00;

            var data = (UInt16)DirectTvSerialCommands.Base + (UInt16)command;

            // Send data as little endian
            byte[] buffer = new byte[2];
            buffer[0] = (byte)(data & MTU_MASK_LO);
            buffer[1] = (byte)((data & MTU_MASK_HI) >> 8);

            _serialPort.Write(buffer, 0, 2);
            Console.WriteLine($"> Writing to serial port: {buffer[0]} {buffer[1]}");
        }
    }
}
