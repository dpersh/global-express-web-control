﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.DirectTV
{
    public interface ISerialPortFactory
    {
        SerialPort GetInstance();
    }
}
