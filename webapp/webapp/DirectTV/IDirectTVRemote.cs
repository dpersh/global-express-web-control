﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.DirectTV
{

    public enum DirectTvChannelDigit : byte
    {
        ChannelSelectDigit0 = 0,
        ChannelSelectDigit1 = 1,
        ChannelSelectDigit2 = 2,
        ChannelSelectDigit3 = 3,
        ChannelSelectDigit4 = 4,
        ChannelSelectDigit5 = 5,
        ChannelSelectDigit6 = 6,
        ChannelSelectDigit7 = 7,
        ChannelSelectDigit8 = 8,
        ChannelSelectDigit9 = 9,
    }

    public interface IDirectTVRemote
    {
        void ChannelUp();
        void ChannelDown();
        void ChannelSet(DirectTvChannelDigit digit);

        void Power();
        void TranInfo();
        void Display();
        void Info();
        void LangSelect();
        void TvRadioToggle();

        void Setup();
        void Guid();
        void Up();
        void Down();
        void Left();
        void Right();
        void Exit();
        void Enter();
        void Menu();
    }
}
