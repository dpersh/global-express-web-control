﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webapp.DirectTV
{ 
    public static class DirectTvSerialCommands
    {
        public static readonly UInt16 Group = 17;
        public static readonly UInt16 Base = 0x240;

        public enum Command : UInt16
        {
            Channel1 = 0,
            Channel2 = 1,
            Channel3 = 2,
            Channel4 = 3,
            Channel5 = 4,
            Channel6 = 5,
            Channel7 = 6,
            Channel8 = 7,
            Channel9 = 8,
            Channel0 = 9,

            ChannelUp = 10,
            ChannelDown = 11,

            Power = 12,
            Setup = 13,

            TranInfo = 14,
            Display = 15,
            Info = 16,

            Guid = 17,
            Up = 18,
            Exit = 19,
            Left = 20,
            Enter = 21,
            Right = 22,
            Menu = 23,
            Down = 24,

            LangSelect = 25,
            TvRadio = 26,
        }
    }
}
